<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="Tree2ListSorted"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchRoot2Head():V"/>
		<constant value="A.__matchTreeNode2ListNode():V"/>
		<constant value="__exec__"/>
		<constant value="Root2Head"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyRoot2Head(NTransientLink;):V"/>
		<constant value="TreeNode2ListNode"/>
		<constant value="A.__applyTreeNode2ListNode(NTransientLink;):V"/>
		<constant value="getAllChildren"/>
		<constant value="MMMIN!Node;"/>
		<constant value="OrderedSet"/>
		<constant value="0"/>
		<constant value="J.append(J):J"/>
		<constant value="children"/>
		<constant value="J.size():J"/>
		<constant value="J.&gt;(J):J"/>
		<constant value="20"/>
		<constant value="26"/>
		<constant value="J.getAllChildren():J"/>
		<constant value="J.union(J):J"/>
		<constant value="9:3-9:15"/>
		<constant value="9:23-9:27"/>
		<constant value="9:3-9:28"/>
		<constant value="8:2-8:6"/>
		<constant value="8:2-8:15"/>
		<constant value="10:6-10:11"/>
		<constant value="10:6-10:20"/>
		<constant value="10:6-10:27"/>
		<constant value="10:30-10:31"/>
		<constant value="10:6-10:31"/>
		<constant value="13:4-13:9"/>
		<constant value="13:17-13:22"/>
		<constant value="13:4-13:23"/>
		<constant value="11:4-11:9"/>
		<constant value="11:16-11:21"/>
		<constant value="11:16-11:38"/>
		<constant value="11:4-11:39"/>
		<constant value="11:47-11:52"/>
		<constant value="11:4-11:53"/>
		<constant value="10:3-14:8"/>
		<constant value="8:2-15:4"/>
		<constant value="child"/>
		<constant value="nodes"/>
		<constant value="__matchRoot2Head"/>
		<constant value="Root"/>
		<constant value="MMIN"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="r"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="h"/>
		<constant value="Head"/>
		<constant value="MMOUT"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="22:3-25:4"/>
		<constant value="__applyRoot2Head"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="root"/>
		<constant value="4"/>
		<constant value="CJ.isEmpty():B"/>
		<constant value="49"/>
		<constant value="I.&gt;(I):B"/>
		<constant value="45"/>
		<constant value="I.+(I):I"/>
		<constant value="QJ.insertAt(IJ):QJ"/>
		<constant value="51"/>
		<constant value="QJ.append(J):QJ"/>
		<constant value="23:12-23:13"/>
		<constant value="23:12-23:18"/>
		<constant value="23:4-23:18"/>
		<constant value="24:13-24:14"/>
		<constant value="24:13-24:19"/>
		<constant value="24:13-24:36"/>
		<constant value="24:53-24:56"/>
		<constant value="24:53-24:62"/>
		<constant value="24:13-24:63"/>
		<constant value="24:4-24:63"/>
		<constant value="var"/>
		<constant value="link"/>
		<constant value="__matchTreeNode2ListNode"/>
		<constant value="Node"/>
		<constant value="s"/>
		<constant value="t"/>
		<constant value="32:3-34:4"/>
		<constant value="__applyTreeNode2ListNode"/>
		<constant value="33:13-33:14"/>
		<constant value="33:13-33:20"/>
		<constant value="33:4-33:20"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<operation name="5">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="7"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="9"/>
			<pcall arg="10"/>
			<dup/>
			<push arg="11"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<pcall arg="10"/>
			<pcall arg="13"/>
			<set arg="3"/>
			<getasm/>
			<push arg="14"/>
			<push arg="8"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="15"/>
			<getasm/>
			<pcall arg="16"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="18">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<getasm/>
			<get arg="3"/>
			<call arg="20"/>
			<if arg="21"/>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<dup/>
			<call arg="23"/>
			<if arg="24"/>
			<load arg="19"/>
			<call arg="25"/>
			<goto arg="26"/>
			<pop/>
			<load arg="19"/>
			<goto arg="27"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<iterate/>
			<store arg="29"/>
			<getasm/>
			<load arg="29"/>
			<call arg="30"/>
			<call arg="31"/>
			<enditerate/>
			<call arg="32"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="33" begin="23" end="27"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="34" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="35">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
			<parameter name="29" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="6"/>
			<lve slot="1" name="34" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="40"/>
			<getasm/>
			<pcall arg="41"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="42">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="43"/>
			<call arg="44"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="45"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="46"/>
			<call arg="44"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="47"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="5" end="8"/>
			<lve slot="1" name="33" begin="15" end="18"/>
			<lve slot="0" name="17" begin="0" end="19"/>
		</localvariabletable>
	</operation>
	<operation name="48">
		<context type="49"/>
		<parameters>
		</parameters>
		<code>
			<push arg="50"/>
			<push arg="8"/>
			<new/>
			<load arg="51"/>
			<call arg="52"/>
			<store arg="19"/>
			<load arg="51"/>
			<get arg="53"/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="53"/>
			<call arg="54"/>
			<pushi arg="51"/>
			<call arg="55"/>
			<if arg="56"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="52"/>
			<goto arg="57"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="58"/>
			<call arg="59"/>
			<load arg="29"/>
			<call arg="52"/>
			<store arg="19"/>
			<enditerate/>
			<load arg="19"/>
		</code>
		<linenumbertable>
			<lne id="60" begin="0" end="2"/>
			<lne id="61" begin="3" end="3"/>
			<lne id="62" begin="0" end="4"/>
			<lne id="63" begin="6" end="6"/>
			<lne id="64" begin="6" end="7"/>
			<lne id="65" begin="10" end="10"/>
			<lne id="66" begin="10" end="11"/>
			<lne id="67" begin="10" end="12"/>
			<lne id="68" begin="13" end="13"/>
			<lne id="69" begin="10" end="14"/>
			<lne id="70" begin="16" end="16"/>
			<lne id="71" begin="17" end="17"/>
			<lne id="72" begin="16" end="18"/>
			<lne id="73" begin="20" end="20"/>
			<lne id="74" begin="21" end="21"/>
			<lne id="75" begin="21" end="22"/>
			<lne id="76" begin="20" end="23"/>
			<lne id="77" begin="24" end="24"/>
			<lne id="78" begin="20" end="25"/>
			<lne id="79" begin="10" end="25"/>
			<lne id="80" begin="0" end="28"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="81" begin="9" end="26"/>
			<lve slot="1" name="82" begin="5" end="28"/>
			<lve slot="0" name="17" begin="0" end="28"/>
		</localvariabletable>
	</operation>
	<operation name="83">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="84"/>
			<push arg="85"/>
			<findme/>
			<push arg="86"/>
			<call arg="87"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="88"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="43"/>
			<pcall arg="89"/>
			<dup/>
			<push arg="90"/>
			<load arg="19"/>
			<pcall arg="91"/>
			<dup/>
			<push arg="92"/>
			<push arg="93"/>
			<push arg="94"/>
			<new/>
			<pcall arg="95"/>
			<pusht/>
			<pcall arg="96"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="97" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="90" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="98">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="99"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="90"/>
			<call arg="100"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="92"/>
			<call arg="101"/>
			<store arg="102"/>
			<load arg="102"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="29"/>
			<get arg="103"/>
			<call arg="58"/>
			<iterate/>
			<store arg="104"/>
			<dup/>
			<call arg="105"/>
			<if arg="106"/>
			<dup/>
			<pushi arg="19"/>
			<swap/>
			<iterate/>
			<load arg="104"/>
			<swap/>
			<store arg="104"/>
			<load arg="104"/>
			<get arg="34"/>
			<swap/>
			<store arg="104"/>
			<load arg="104"/>
			<get arg="34"/>
			<call arg="107"/>
			<if arg="108"/>
			<pushi arg="19"/>
			<call arg="109"/>
			<enditerate/>
			<load arg="104"/>
			<call arg="110"/>
			<goto arg="111"/>
			<load arg="104"/>
			<call arg="112"/>
			<enditerate/>
			<call arg="30"/>
			<set arg="82"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="113" begin="11" end="11"/>
			<lne id="114" begin="11" end="12"/>
			<lne id="115" begin="9" end="14"/>
			<lne id="116" begin="20" end="20"/>
			<lne id="117" begin="20" end="21"/>
			<lne id="118" begin="20" end="22"/>
			<lne id="119" begin="35" end="35"/>
			<lne id="120" begin="35" end="36"/>
			<lne id="119" begin="39" end="39"/>
			<lne id="120" begin="39" end="40"/>
			<lne id="121" begin="17" end="51"/>
			<lne id="122" begin="15" end="53"/>
			<lne id="97" begin="8" end="54"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="4" name="123" begin="24" end="50"/>
			<lve slot="3" name="92" begin="7" end="54"/>
			<lve slot="2" name="90" begin="3" end="54"/>
			<lve slot="0" name="17" begin="0" end="54"/>
			<lve slot="1" name="124" begin="0" end="54"/>
		</localvariabletable>
	</operation>
	<operation name="125">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="126"/>
			<push arg="85"/>
			<findme/>
			<push arg="86"/>
			<call arg="87"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="88"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="46"/>
			<pcall arg="89"/>
			<dup/>
			<push arg="127"/>
			<load arg="19"/>
			<pcall arg="91"/>
			<dup/>
			<push arg="128"/>
			<push arg="126"/>
			<push arg="94"/>
			<new/>
			<pcall arg="95"/>
			<pusht/>
			<pcall arg="96"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="129" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="127" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="130">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="99"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="127"/>
			<call arg="100"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="128"/>
			<call arg="101"/>
			<store arg="102"/>
			<load arg="102"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="34"/>
			<call arg="30"/>
			<set arg="34"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="131" begin="11" end="11"/>
			<lne id="132" begin="11" end="12"/>
			<lne id="133" begin="9" end="14"/>
			<lne id="129" begin="8" end="15"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="128" begin="7" end="15"/>
			<lve slot="2" name="127" begin="3" end="15"/>
			<lve slot="0" name="17" begin="0" end="15"/>
			<lve slot="1" name="124" begin="0" end="15"/>
		</localvariabletable>
	</operation>
</asm>
